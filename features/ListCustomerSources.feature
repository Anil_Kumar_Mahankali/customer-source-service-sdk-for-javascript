Feature: List Customer Sources
  Lists all customer sources

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listCustomerSources
    Then all customer sources in the customer-source-service are returned