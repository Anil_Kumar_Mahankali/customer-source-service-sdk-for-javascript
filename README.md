## Description
Precor Connect customer source service SDK for javascript.

## Features

##### List Customer Sources
* [documentation](features/ListCustomerSources.feature)

## Setup

**install via jspm**  
```shell
jspm install customer-source-service-sdk=bitbucket:precorconnect/customer-source-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import CustomerSourceServiceSdk,{CustomerSourceServiceSdkConfig} from 'customer-source-service-sdk'

const customerSourceServiceSdkConfig = 
    new CustomerSourceServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const customerSourceServiceSdk = 
    new CustomerSourceServiceSdk(
        customerSourceServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```