import CustomerSourceServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be CustomerSourceServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new CustomerSourceServiceSdk(config.customerSourceServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(CustomerSourceServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listCustomerSources method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerSourceServiceSdk(config.customerSourceServiceSdkConfig);


                /*
                 act
                 */
                const customerSourcesPromise =
                    objectUnderTest
                        .listCustomerSources(
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                customerSourcesPromise
                    .then((customerSources) => {
                        expect(customerSources.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000)
        });
    });
});